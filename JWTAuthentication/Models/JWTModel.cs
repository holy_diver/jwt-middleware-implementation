using Newtonsoft.Json;

namespace JWTAuthentication.Models
{
    public class JWTModel
    {
        [JsonProperty("iat")]
        public long IssuedAt { get; set; }

        [JsonProperty("sub")]
        public string Subject { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("exp")]
        public long Expires { get; set; }
    }
}