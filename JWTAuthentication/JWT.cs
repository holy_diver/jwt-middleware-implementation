﻿using System;
using System.Buffers.Text;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using JWTAuthentication.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace JWTAuthentication
{
    public class JWT
    {
        private readonly RequestDelegate _next;

        public JWT(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
                var attribute = endpoint?.Metadata.GetMetadata<JWTAttribute>();
                if (attribute != null)
                {
                    var jwtPass = attribute.Password;
                    var authHeader = context.Request.Headers["Authorization"];
                    var jwt = authHeader[0].Replace("Bearer ", "").Split('.');
                    var jwtHeader = jwt[0];
                    var jwtPayload =
                        JsonConvert.DeserializeObject<JWTModel>(
                            System.Text.Encoding.UTF8.GetString(Base64UrlTextEncoder.Decode(jwt[1])));
                    var jwtSignature = jwt[2];

                    var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(jwtPass));
                    var hmacHash = hmac.ComputeHash(Encoding.ASCII.GetBytes(jwt[0] + '.' + jwt[1]));
                    var base64HmacHash = Base64UrlTextEncoder.Encode(hmacHash);

                    if (base64HmacHash != jwtSignature)
                    {
                        context.Response.StatusCode = 401;              
                        await context.Response.WriteAsync("JWT Signature Mismatch!");
                        return;
                    }

                    if (DateTimeOffset.FromUnixTimeSeconds(jwtPayload.Expires).Date < DateTime.Now)
                    {
                        context.Response.StatusCode = 401;                
                        await context.Response.WriteAsync("JWT Expired!");
                        return;
                    }
                }
                await _next(context);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = 400; //Bad Request                
                await context.Response.WriteAsync("JWT Authentication Error");
            }
        }
    }
}