using Microsoft.AspNetCore.Builder;

namespace JWTAuthentication
{
    public static class JWTMiddlewareExtensions
        {
            public static IApplicationBuilder UseJWT(
                this IApplicationBuilder builder)
            {
                return builder.UseMiddleware<JWT>();
            }
        }
    }